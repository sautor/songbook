# Sautor Songbook

Extensão de cânticos para o sistema Sautor.

## Instalação

Instalar via Composer:

````sh
composer require sautor/songbook
````

No final do ficheiro `resources/scss/app.scss`:

````scss
// Addons
@import "@sautor/songbook/resources/scss/styles";
````

No ficheiro `resources/js/app.js`:

````js
// Na linha 4, importar também a função registerComponents
import initVue, { registerComponents } from '@sautor/core/resources/js/bootstrap/vue'

// Importar os componentes
import songbookComponents from '@sautor/songbook/resources/js/components'

// Antes da chamada à função initVue, registar o componente
registerComponents(Vue, songbookComponents)
````

Para correr a migração:
````sh
php artisan migrate
````
