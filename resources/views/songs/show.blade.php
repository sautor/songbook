@extends('songbook::layouts.main')

@section('sb.content')

    <div class="{{ empty($song->chords) ? 'sautor-addon__container' : 'sautor-addon__full-container' }}">


      <header class="sautor-addon__header">
        <div class="s-sb-flex-1">
          <h1 class="sautor-addon__header__title">
            {{ $song->title }}
            @if($song->by)
              <small>{{ $song->by }}</small>
            @endif
          </h1>
          @unless($song->tags->isEmpty() and !$song->grupo)
            <p class="s-sb-text-sm s-sb-text-gray-500 s-sb-mt-3 s-sb-ml-1">
              @if($song->grupo)
                <span class="fas fa-users s-sb-text-gray-400 s-sb-mr-1 s-sb-text-xs"></span>
                <span class="s-sb-mr-3">{{ $song->grupo->nome }}</span>
              @endif
              @unless($song->tags->isEmpty())
                <span class="fas fa-tag s-sb-text-gray-400 s-sb-mr-1 s-sb-text-xs"></span>
                <span class="s-sb-mr-3">{{ $song->tags->pluck('name')->join(', ') }}</span>
              @endunless
            </p>
          @endunless
        </div>

        @can('update', \Sautor\Songbook\Models\Song::class)
          <div class="sautor-addon__header__actions">
            <a href="{{ route('songbook.canticos.edit', $song) }}" class="button button--responsive">
              <span class="fas fa-pencil"></span>
              Editar
            </a>
          </div>
        @endcan
      </header>


      <div class="s-sb-grid s-sb-grid-cols-1 @unless(empty($song->chords)) md:s-sb-grid-cols-2 @endunless s-sb-gap-8">
        <div>
          <h3 class="title title--sm s-sb-mb-3">Letra</h3>
            <div class="s-sb-bg-white s-sb-px-6 s-sb-py-4 s-sb-rounded s-sb-shadow song-content">
              {!! $song->lyrics !!}
            </div>
        </div>
        @unless(empty($song->chords))
        <div>
          <h3 class="title title--sm s-sb-mb-3">Acordes</h3>
            <div class="s-sb-bg-white s-sb-px-6 s-sb-py-4 s-sb-rounded s-sb-shadow song-content song-content--chords">
              {!! $song->chords !!}
            </div>
        </div>
        @endunless
      </div>

    </div>
@endsection
