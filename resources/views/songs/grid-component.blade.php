@unless($songs->isEmpty())
    <div class="s-sb-columns-3xs -s-sb-m-2">
        @foreach($songs as $song)
            <a @can('view', $song) href="{{ route('songbook.canticos.show', $song) }}" @endcan class="block s-sb-p-4 s-sb-break-inside-avoid hover:s-sb-bg-gray-200">
                <p class="s-sb-font-bold">
                    <span class=" s-sb-underline s-sb-decoration-2 s-sb-decoration-primary-100 @unless(empty($song->chords)) s-sb-pr-5 @endunless ">{{ $song->title }}</span>

                    @unless(empty($song->chords))
                        <span class="fas fa-guitar s-sb-text-primary-300 -s-sb-ml-4"></span>
                    @endunless
                </p>
                <p class="s-sb-font-accent s-sb-text-xs s-sb-text-gray-600">
                    @if($song->by)
                        {{ $song->by }}
                        @if($song->grupo)
                            &middot;
                    @endif
                    @endif
                    @if($song->grupo)
                        {{ $song->grupo->nome_curto }}
                    @endif
                </p>
                @unless($song->tags->isEmpty())
                    <p class="s-sb-text-xs s-sb-text-gray-500 s-sb-mt-2">
                        <span class="fas fa-tag s-sb-text-gray-400"></span>
                        {{ $song->tags->pluck('name')->join(', ') }}
                    </p>
                @endunless
            </a>
        @endforeach
    </div>
@else
    <section class="sautor-list--empty songs-list--empty">
        {{ empty($empty_message) ? 'Não existem cânticos.' : $empty_message }}
    </section>
@endunless
