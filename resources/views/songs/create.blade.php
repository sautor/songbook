@extends('songbook::layouts.main')

@section('sb.content')
  <div class="sautor-addon__full-container">
    <header class="sautor-addon__header">
      <h1 class="sautor-addon__header__title">
        Criar cântico
      </h1>
    </header>

      @php($scope = 'new-song')
      <form action="{{ route('songbook.canticos.store') }}" method="POST" data-vv-scope="{{ $scope }}">
        @csrf

        @include('songbook::songs.form')

        <div class="sautor-addon__actions">
          <button type="submit" class="button button--primary">Criar</button>
        </div>

      </form>
    </div>
@endsection
