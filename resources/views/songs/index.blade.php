@extends('songbook::layouts.main')

@section('sb.content')
    <div class="sautor-addon__full-container">

        <header class="sautor-addon__header">
            <h1 class="sautor-addon__header__title">
                Cânticos
                <small>{{ $songs->count() }}</small>
            </h1>
            @can('create', \Sautor\Songbook\Models\Song::class)
                <div class="sautor-addon__header__actions">
                    <a href="{{ route('songbook.canticos.create') }}" class="button button--responsive">
                        <span class="far fa-plus"></span>
                        Criar
                    </a>
                </div>
            @endcan
        </header>

        <form method="GET">
            <div class="control s-sb-mb-4">

                <div class="control__addon control__addon--before">
                    <span class="control__addon__text"><span class="far fa-search"></span></span>
                </div>

                <input class="input" placeholder="Procurar..." name="search" id="search" type="search"
                       value="{{ request()->search }}"
                />

            </div>

        </form>

        @if($isSearch)
            <div class="s-sb-grid s-sb-grid-cols-1 md:s-sb-grid-cols-[auto_minmax(0,_1fr)] s-sb-gap-12 s-sb-mt-12">
                <div>
                    <p class="s-sb-font-headline s-sb-text-xl s-sb-text-gray-700">
                        Procura por
                        <strong class="s-sb-block s-sb-text-3xl s-sb-text-primary-700">{{ request()->search }}</strong>
                    </p>
                </div>
                @include('songbook::songs.grid-component')
            </div>
        @else
            <div class="s-sb-grid s-sb-grid-cols-[auto_minmax(0,_1fr)] s-sb-gap-x-8 s-sb-gap-y-24 s-sb-mt-12">
                @foreach($grouped_songs as $letter => $group_songs)
                    <div>
                        <div class="s-sb-font-headline s-sb-text-6xl s-sb-text-primary-700 text-center s-sb-sticky s-sb-top-4">{{ $letter }}</div>

                    </div>
                    @include('songbook::songs.grid-component', ['songs' => $group_songs])
                @endforeach
            </div>
        @endif

    </div>
@endsection
