@unless($songs->isEmpty())
  <section class="sautor-list sautor-list--full songs-list">
    @foreach($songs as $song)
      <div class="sautor-list__item">
        <div class="sautor-list__item__logo">
          <span class="sautor-list__item__logo__icon">
              <span class="fas fa-music-alt"></span>
          </span>
        </div>
        <a @can('view', $song) href="{{ route('songbook.canticos.show', $song) }}" @endcan class="sautor-list__item__data">
          <p class="sautor-list__item__name">
            {{ $song->title }}
            @foreach($song->tags as $tag)
              @include('songbook::tags.single-component')
            @endforeach
          </p>
          <p class="sautor-list__item__meta">
            @unless(empty($song->chords))
              <span class="far fa-guitar"></span>
              @if($song->by or $song->grupo)
                &middot;
              @endif
            @endunless
            @if($song->by)
              {{ $song->by }}
              @if($song->grupo)
                &middot;
              @endif
            @endif
            @if($song->grupo)
              {{ $song->grupo->nome_curto }}
            @endif
          </p>
        </a>

        @unless((empty($actions) or empty($a = $actions($song))) and (Auth::guest() or !Auth::user()->can('view', $song)))
          <div class="sautor-list__item__actions">
            @unless(empty($actions) or empty($a))
              @foreach($a as $action)
                <a title="{{ $action['label'] }}"
                   @if(!empty($action['link']))
                   href="{{ $action['link'] }}"
                   @elseif(!empty($action['modal']))
                   href="#" data-toggle="modal" data-target="#{{ $action['modal'] }}"
                    @endif
                >
                  <span class="{{ $action['icon'] }}"></span>
                </a>
              @endforeach
            @endunless
            @can('view', $song)
              <a title="Ver" href="{{ route('songbook.canticos.show', $song) }}">
                <span class="far fa-angle-right"></span>
              </a>
            @endcan
          </div>
        @endunless
      </div>
    @endforeach
  </section>
@else
  <section class="sautor-list--empty songs-list--empty">
    {{ empty($empty_message) ? 'Não existem cânticos.' : $empty_message }}
  </section>
@endunless
