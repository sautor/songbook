<x-forms.input name="title" label="Título" :value="isset($song) ? $song->title : null" required :scope="$scope" />
<x-forms.input name="by" label="Por" :value="isset($song) ? $song->by : null" :scope="$scope" />

<div class="s-sb-grid s-sb-grid-cols-1 md:s-sb-grid-cols-2 s-sb-gap-8">
  <div>
    <div class="field">
      <label class="label">Letra</label>
      <div class="control flex-col">
        <small-content-editor name="lyrics" class="sce-editor--song" value="{{ isset($song) ? $song->lyrics : null }}" simple></small-content-editor>
      </div>
    </div>
  </div>
  <div>
    <div class="field">
      <label class="label">Acordes</label>
      <div class="control s-sb-flex-col">
        <small-content-editor name="chords" class="sce-editor--song sce-editor--chords" value="{{ isset($song) ? $song->chords : null }}" simple></small-content-editor>
      </div>
    </div>
  </div>
</div>

<div class="field">
  <label class="label">
    Grupo
  </label>
  <div class="control s-sb-flex-col">
    <grupo-picker name="grupo_id" with-input visiveis value="{{ old('grupo_id', (isset($song) ? $song->grupo_id : null)) }}"></grupo-picker>
  </div>
</div>

<div class="field @if($errors->has('tags.*')) field--invalid @endif ">
  <label class="label">Etiquetas</label>
  <div class="control s-sb-flex-col s-sb-gap-2">
  @foreach($tags as $tag)
      <x-forms.checkbox :name="'tags['.$tag->id.']'" :label="$tag->name" :cb-val="$tag->id" :value="isset($song) ? $song->tags->contains($tag) : false" :scope="$scope" no-field />
  @endforeach
  </div>

  @if ($errors->has('tags.*'))
    <span class="field__help field__help--invalid">
        {{ $errors->first('tags.*') }}
    </span>
  @endif
</div>
