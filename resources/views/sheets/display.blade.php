@php($content = $sheet->preparedContent())

<div class="sb-sheet-display">
  <header class="sb-sheet-display__header">
    <h1 class="title title--lg sb-sheet-display__title">{{ $sheet->title }}</h1>

    @if($content['header']->isNotEmpty())
      @foreach($content['header'] as $item)
        @include('songbook::sheets.display-item')
      @endforeach
    @endif
  </header>

  @unless(empty($slot))
    {{ $slot }}
  @endunless

  <main class="sb-sheet-display__content">
    @foreach($content['main'] as $item)
      @include('songbook::sheets.display-item')
    @endforeach
  </main>

  @if($content['footer']->isNotEmpty())
    <footer class="sb-sheet-display__footer">
      @foreach($content['footer'] as $item)
        @include('songbook::sheets.display-item')
      @endforeach
    </footer>
  @endif

</div>
