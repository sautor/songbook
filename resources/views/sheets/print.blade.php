@extends('resources::layout', ['hideFooter' => true])

@section('title')
  {{ $sheet->title }}
@endsection

@section('content')
  <div class="print__header">
    @include('resources::resources.partials.logo')
    @isset($grupo)
      @include('resources::resources.partials.group-logo')
    @endisset
  </div>

  @include('songbook::sheets.display')
@endsection

@push('styles')
  <link rel="stylesheet" href="{{ asset('addons/songbook/style.css') }}">
@endpush
