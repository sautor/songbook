@extends('songbook::layouts.main')

@section('sb.content')
  <div class="sautor-addon__full-container">
    <header class="sautor-addon__header">
      <h1 class="sautor-addon__header__title">
        Editar folha de cânticos
      </h1>
    </header>

      @php($scope = 'edit-sheet')
      <form action="{{ route('songbook.folhas.update', $sheet) }}" method="POST" data-vv-scope="{{ $scope }}">
        @method('PUT')
        @csrf

        @include('songbook::sheets.form')

        <div class="sautor-addon__actions">
          <button type="submit" class="button button--primary">Editar</button>
          @can('delete', $sheet)
            <button type="button" class="button button--danger-light" @click.prevent="openModal('deleteModal')">
              <span class="far fa-trash mr-2"></span>
              Eliminar
            </button>
          @endcan
        </div>

      </form>

    </div>

  @can('delete', $sheet)
    <modal id="deleteModal">
      <div class="modal__body modal__confirmation">
        <div class="modal__confirmation__icon">
          <span class="fas fa-exclamation"></span>
        </div>
        <div class="modal__confirmation__content">
          <h3 class="modal__confirmation__title">Eliminar cântico</h3>
          <p class="modal__confirmation__text">
            Tem a certeza que quer eliminar a folha <strong>{{ $sheet->title }}</strong>?
          </p>
        </div>
      </div>
      <form class="modal__footer" method="POST" action="{{ route('songbook.folhas.destroy', $sheet) }}">
        @csrf
        @method('DELETE')
        <button type="submit" class="button button--danger">Eliminar</button>
        <button type="button" class="button" @click.prevent="closeModal('deleteModal')">Cancelar</button>
      </form>
    </modal>
  @endcan
@endsection
