<x-forms.input name="title" label="Título" :value="isset($sheet) ? $sheet->title : null" required :scope="$scope" />

<div class="s-sb-grid s-sb-grid-cols-1 md:s-sb-grid-cols-2 s-sb-gap-8">
  <div >
    <div class="field @if($errors->has('datetimes.*')) field--invalid @endif ">
      <label class="label">Datas</label>
      <div class="control">
        <s-sb-date-collection-input class="flex-1" name="datetimes" value="{{ old('datetimes', (isset($sheet) ? $sheet->dates->map(fn ($date) => $date->toDateTimeLocalString())->toJson() : '[]')) }}" />
      </div>
      @if ($errors->has('datetimes.*'))
        <span class="field__help field__help--invalid">
          {{ $errors->first('datetimes.*') }}
        </span>
      @endif
    </div>
  </div>
  <div>
    <div class="field">
      <label class="label">
        Grupo
      </label>
      <div class="control">
        <grupo-picker name="grupo_id" with-input visiveis value="{{ old('grupo_id', (isset($sheet) ? $sheet->grupo_id : null)) }}" class="s-sb-flex-1"></grupo-picker>
      </div>
    </div>

    <x-forms.checkbox name="is-public" label="Pública" :value="isset($sheet) ? $sheet->is_public : false" :scope="$scope" />
  </div>
</div>

<div class="field @if($errors->has('content')) field--invalid @endif ">
  <label class="label">Conteúdo</label>
  <div class="control">
    <s-sb-sheet-editor name="content" value="{{ old('content', (isset($formModel) ? json_encode($formModel['content']) : '[]')) }}" class="flex-1"></s-sb-sheet-editor>
  </div>
  @if ($errors->has('content'))
    <span class="field__help field__help--invalid">
        {{ $errors->first('content') }}
    </span>
  @endif
</div>

@push('scripts')
  <script src="{{ asset('addons/songbook/components.js') }}" type="module"></script>
@endpush
