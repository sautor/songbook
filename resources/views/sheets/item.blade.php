<div class="sautor-list__item">
  @if($item['type'] === 'song')
    <div class="sautor-list__item__logo">
      <span class="sautor-list__item__logo__icon sautor-list__item__logo__icon--strong">
        <span class="fas fa-music-alt"></span>
      </span>
    </div>
    <a href="{{ route('songbook.canticos.show', $item['song']) }}" class="sautor-list__item__data">
      <p class="sautor-list__item__name">
        {{ $item['song']->title }}
      </p>
      @if(array_key_exists('label',$item))
        <p class="sautor-list__item__meta">
          {{ $item['label'] }}
        </p>
      @endif
    </a>
  @elseif($item['type'] === 'heading')
    <div class="sautor-list__item__logo">
      <span class="sautor-list__item__logo__icon sautor-list__item__logo__icon--strong">
        <span class="fas fa-heading"></span>
      </span>
    </div>
    <div class="sautor-list__item__data">
      <p class="sautor-list__item__name">
        {{ $item['content'] }}
      </p>
      <p class="sautor-list__item__meta">
        Nível {{ $item['level'] }}
      </p>
    </div>
  @elseif($item['type'] === 'groups')
    <div class="sautor-list__item__logo">
      <span class="sautor-list__item__logo__icon sautor-list__item__logo__icon--strong">
        <span class="fas fa-users"></span>
      </span>
    </div>
    <div class="sautor-list__item__data">
      <p class="sautor-list__item__name">
        Lista de grupos
      </p>
      <p class="sautor-list__item__meta">
        {{ $item['groups']->pluck('nome_curto')->join(', ') }}
      </p>
    </div>
  @else
    <div class="sautor-list__item__logo">
      <span class="sautor-list__item__logo__icon sautor-list__item__logo__icon--strong">
        <span class="fas fa-question"></span>
      </span>
    </div>
    <div class="sautor-list__item__data">
      <p class="sautor-list__item__name">Desconhecido</p>
    </div>
  @endif
</div>
