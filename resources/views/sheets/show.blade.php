@extends('songbook::layouts.main')

@section('sb.content')
  <div class="sautor-addon__container">

    <header class="sautor-addon__header">
      <h1 class="sautor-addon__header__title">
        {{ $sheet->title }}
        <small>Folha de cânticos</small>
      </h1>
        <div class="sautor-addon__header__actions">
          <dropdown>
            <template v-slot:default="{ toggle }">
              <button type="button" class="button button--responsive" @click="toggle">
                <span class="far fa-eye"></span>
                Ver
              </button>
            </template>

            <template v-slot:dropdown>
              <a href="{{ route('songbook.public.show', $sheet) }}" class="dropdown__item">
                <span class="dropdown__item__icon far fa-fw fa-eye"></span>
                Ver
              </a>
              <a href="{{ route('songbook.public.show', $sheet) }}?acordes" class="dropdown__item">
                <span class="dropdown__item__icon far fa-fw fa-guitar"></span>
                Ver acordes
              </a>

              @if($sheet->hasMedia('pdf') or $sheet->hasMedia('pdf-chords'))
                <div class="dropdown__divider dropdown__divider--with-icon"></div>

              @if($sheet->hasMedia('pdf'))
                    <a href="{{ $sheet->getFirstMediaUrl('pdf') }}" class="dropdown__item">
                      <span class="dropdown__item__icon far fa-fw fa-file-pdf"></span>
                      PDF
                    </a>
                  @endif
                  @if($sheet->hasMedia('pdf-chords'))
                    <a href="{{ $sheet->getFirstMediaUrl('pdf-chords') }}" class="dropdown__item">
                      <span class="dropdown__item__icon fa-layers fa-fw">
                         <span class="far fa-file"></span>
                         <span class="fas fa-guitar" data-fa-transform="shrink-10 down-2"></span>
                      </span>
                      PDF Acordes
                    </a>
                  @endif
              @endif
            </template>
          </dropdown>

          @can('update', $sheet)
          <a href="{{ route('songbook.folhas.edit', $sheet) }}" class="button button--responsive">
            <span class="far fa-pencil"></span>
            Editar
          </a>
          @endcan

        </div>
    </header>

      <div class="s-sb-grid s-sb-grid-cols-1 md:s-sb-grid-cols-2 s-sb-gap-8">
        <div>

          @if($sheet->dates->isNotEmpty())
            <h5 class="title title--sm s-sb-mb-2">Datas</h5>
          <div class="prose">
            <ul>
              @foreach($sheet->dates as $date)
                <li>{{ $date->isoFormat('LLL') }}</li>
              @endforeach
            </ul>
          </div>

          @endif

          @if($sheet->grupo)
              @if($sheet->grupo->logo)
                <img src="{{ $sheet->grupo->logo }}" class="s-sb-w-16 s-sb-float-right" style="width: 4rem;">
              @endif
                <h5 class="title title--sm s-sb-mb-2">Grupo</h5>
                {{ $sheet->grupo->nome }}
          @endif




        </div>
        <div>

          <h5 class="title title--sm s-sb-mb-2">Conteúdo</h5>

          @if($content['header']->isNotEmpty())
            <h6 class="title title--xs s-sb-mb-1">Cabeçalho</h6>
            <div class="sautor-list s-sb-mb-3">
              @foreach($content['header'] as $item)
                @include('songbook::sheets.item')
              @endforeach
            </div>
          @endif

          @if($content['main']->isNotEmpty())
            <h6 class="title title--xs s-sb-mb-1">Conteúdo</h6>
            <div class="sautor-list s-sb-mb-3">
              @foreach($content['main'] as $item)
                @include('songbook::sheets.item')
              @endforeach
            </div>
          @endif

          @if($content['footer']->isNotEmpty())
            <h6 class="title title--xs s-sb-mb-1">Rodapé</h6>
            <div class="sautor-list s-sb-mb-3">
              @foreach($content['footer'] as $item)
                @include('songbook::sheets.item')
              @endforeach
            </div>
          @endif

        </div>
      </div>

  </div>
@endsection
