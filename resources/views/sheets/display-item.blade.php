<article class="sb-sheet-display__item sb-sheet-display__item--{{ $item['type'] }} @if($item['type'] === 'song') sb-sheet-display__item--song-{{ $item['id'] }} @endif ">

  @if($item['type'] === 'song')
    <header class="sb-sheet-display__item__header">
      @if(array_key_exists('label', $item))
        <small>{{ $item['label'] }}</small>
      @endif
      <h5 class="title title--xs">{{ $item['song']->title }}</h5>
      @if($item['song']->by)
        <em>{{ $item['song']->by }}</em>
      @endif
    </header>
    @if($chords and !empty($item['song']->chords))
      <div class="sb-sheet-display__chords">{!! $item['song']->chords !!}</div>
    @else
      <div class="sb-sheet-display__song">{!! $item['song']->lyrics !!}</div>
    @endif

    @unless(empty($item['hide']))
      @push('styles')
        <style>
          @foreach($item['hide'] as $p)
            .sb-sheet-display__item--song-{{ $item['id'] }} p:nth-of-type({{ $p }}) {
              display: none;
            }
          @endforeach
        </style>
      @endpush
    @endunless

  @endif

  @if($item['type'] === 'heading')
    @php($tag = 'h'.$item['level'])
    <{{ $tag }}>
      {{ $item['content'] }}
    </{{ $tag }}>
  @endif

  @if($item['type'] === 'groups')
      @foreach($item['groups'] as $group)
        <a href="{{ route('grupo.index', $group) }}" class="sb-sheet-display__group">
          @if($group->logo)
            <img src="{{ $group->logo }}" alt="{{ $group->nome_curto }}">
          @else
            {{ $group->nome_curto }}
          @endif
        </a>
      @endforeach
  @endif

</article>
