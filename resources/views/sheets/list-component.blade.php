@unless($sheets->isEmpty())
  <section class="sautor-list sheets-list">
    @foreach($sheets as $sheet)
      <div class="sautor-list__item">
        <div class="sautor-list__item__logo">
          @if($sheet->grupo and $sheet->grupo->logo)
            <img src="{{ $sheet->grupo->logo }}" alt="{{ $sheet->grupo->nome }}">
          @else
            <span class="sautor-list__item__logo__icon sautor-list__item__logo__icon--strong">
                <span class="fas fa-list-music"></span>
            </span>
          @endif
        </div>
        <a href="{{ route('songbook.folhas.show', $sheet) }}" class="sautor-list__item__data">
          <p class="sautor-list__item__name">
            {{ $sheet->title }}
          </p>
          <p class="sautor-list__item__meta">
            {{ $sheet->dates->map(function($date) { return $date->isoFormat('lll'); })->join(', ') }}
            @if($sheet->grupo)
            &middot; {{ $sheet->grupo->nome }}
            @endif
          </p>
        </a>

        <div class="sautor-list__item__actions">
          @unless(empty($actions) or empty($a = $actions($sheet)))
            @foreach($a as $action)
              <a title="{{ $action['label'] }}"
                 @if(!empty($action['link']))
                 href="{{ $action['link'] }}"
                 @elseif(!empty($action['modal']))
                 href="#" data-toggle="modal" data-target="#{{ $action['modal'] }}"
                      @endif
              >
                <span class="{{ $action['icon'] }}"></span>
              </a>
            @endforeach
          @endunless
          <a title="Ver" href="{{ route('songbook.folhas.show', $sheet) }}">
            <span class="far fa-angle-right"></span>
          </a>
        </div>
      </div>
    @endforeach
  </section>
@else
  <section class="sautor-list--empty tags-list--empty">
    {{ empty($empty_message) ? 'Não existem folhas de cânticos.' : $empty_message }}
  </section>
@endunless
