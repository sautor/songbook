@extends('songbook::layouts.main')

@section('sb.content')
  <div class="sautor-addon__full-container">
    <header class="sautor-addon__header">
      <h1 class="sautor-addon__header__title">
        Criar folha de cânticos
      </h1>
    </header>

      @php($scope = 'new-sheet')
      <form action="{{ route('songbook.folhas.store') }}" method="POST" data-vv-scope="{{ $scope }}">
        @csrf

        @include('songbook::sheets.form')

        <div class="sautor-addon__actions">
          <button type="submit" class="button button--primary">Criar</button>
        </div>
      </form>
  </div>
@endsection
