@extends('layouts.app')
@section('content')
  @include('songbook::layouts.partials.header')
  @yield('sb.content')
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/songbook/style.css') }}">
@endpush
