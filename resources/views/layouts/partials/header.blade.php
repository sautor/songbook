<x-addon-header name="Cânticos" icon="fas fa-music">
  @can('gerir cânticos')
    <x-slot:nav>
      <a href="{{ route('songbook.canticos.index') }}">
        <span class="fas fa-music-alt"></span>
        Cânticos
      </a>
      <a href="{{ route('songbook.etiquetas.index') }}">
        <span class="fas fa-tag"></span>
        Etiquetas
      </a>
      <a href="{{ route('songbook.folhas.index') }}">
        <span class="fas fa-list-music"></span>
        Folhas
      </a>
    </x-slot:nav>
  @endcan
</x-addon-header>
