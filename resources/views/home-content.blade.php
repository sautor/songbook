@php($sheets = \Sautor\Songbook\Models\Sheet::nextWeek()->public()->get())
@unless($sheets->isEmpty())
  @php($allDates = $sheets->pluck('dates')->flatten())

  <div class="s-sb-container s-sb-mx-auto s-sb-px-8 s-sb-pt-16">
    <div class="s-sb-rounded-lg s-sb-drop-shadow-lg s-sb-bg-gray-800 s-sb-text-white s-sb-px-6 s-sb-py-4 s-sb-flex s-sb-flex-col md:s-sb-flex-row md:s-sb-items-center s-sb-gap-4">
      <div class="s-sb-rounded-full s-sb-w-10 s-sb-h-10 s-sb-bg-gray-600 s-sb-flex s-sb-items-center s-sb-justify-center">
        <span class="fas fa-music s-sb-text-gray-50 s-sb-scale-125"></span>
      </div>

      <div class="s-sb-flex-1">
          <h2 class="s-sb-font-headline s-sb-text-white s-sb-text-lg">Cânticos</h2>

          <p>
            Estão disponíveis os <strong>cânticos</strong> para
            {{ $allDates->map(function ($date) { return lcfirst($date->calendar()); })->join(', ', ' e ') }}.
          </p>
        </div>

      <a href="{{ route('songbook.index') }}" class="s-sb-inline-block s-sb-rounded s-sb-px-6 s-sb-py-2 s-sb-font-accent s-sb-border-2 s-sb-border-white s-sb-self-end md:s-sb-self-center hover:s-sb-text-gray-800 hover:s-sb-bg-white s-sb-transition-colors s-sb-duration-300">
        Ver
        <span class="far fa-arrow-right ml-1"></span>
      </a>

    </div>
  </div>
@endunless


@push('styles')
  <link rel="stylesheet" href="{{ asset('addons/songbook/style.css') }}">
@endpush
