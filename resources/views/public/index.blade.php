@extends('songbook::layouts.main')

@section('sb.content')


  <div class="sautor-addon__container">

    @if($sheets->isEmpty())

      VAZIO!

    @else

    <div class="s-sb-grid md:s-sb-grid-cols-2 lg:s-sb-grid-cols-3 s-sb-gap-4">

      @foreach($sheets as $sheet)

          <div class="s-sb-bg-white s-sb-rounded-lg s-sb-shadow s-sb-p-6 s-sb-h-full s-sb-flex s-sb-flex-col">
              <h5 class="title title--xs">{{ $sheet->title }}</h5>
              @if($sheet->grupo)
                <h6 class="s-sb-text-gray-600">{{ $sheet->grupo->nome_curto }}</h6>
              @endif

              <div class="s-sb-flex-1 s-sb-mt-4">
                @foreach($sheet->dates as $date)
                  <p class="s-sb-leading-tight">
                    <strong>{{ ucfirst($date->isoFormat('dddd')) }}</strong>,
                    {{ $date->isoFormat('LT') }}<br />
                    <small class="s-sb-text-sm s-sb-text-gray-600">{{ $date->isoFormat('LL') }}</small>
                  </p>
                @endforeach
              </div>

              <div class="s-sb-flex s-sb-items-center s-sb-mt-4 s-sb-gap-2">
                <a href="{{ route('songbook.public.show', $sheet) }}" class="button button--primary">
                  Ver <span class="s-sb-ml-2 far fa-arrow-right"></span>
                </a>
                @if($sheet->hasMedia('pdf'))
                  <a href="{{ $sheet->getFirstMediaUrl('pdf') }}" class="button">
                    <span class="far fa-file-pdf s-sb-mr-1"></span> PDF
                  </a>
                @endif

                @canany(['gerir cânticos', 'ver acordes'])
                  <div class="dropdown s-sb-ml-auto">
                    <button class="btn btn-light" data-toggle="dropdown">
                      <span class="fas fa-guitar"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                      <a class="dropdown-item" href="{{ route('songbook.public.show', $sheet) }}?acordes">
                        <span class="far fa-fw fa-eye mr-1 text-primary"></span>
                        Ver
                      </a>
                      @if($sheet->hasMedia('pdf-chords'))
                        <a href="{{ $sheet->getFirstMediaUrl('pdf-chords') }}" class="dropdown-item">
                          <span class="far fa-fw fa-file-pdf mr-1 text-primary"></span>
                          PDF
                        </a>
                      @endif
                    </div>
                  </div>
                @endcanany

              </div>


            </div>

      @endforeach

    </div>

    @endif

  </div>

@endsection
