@extends('songbook::layouts.main')

@section('sb.content')

  <div class="sautor-addon__container">

    @component('songbook::sheets.display', ['sheet' => $sheet, 'chords' => $chords])
      @if($chords)
        <p class="text-center">
          <a href="{{ route('songbook.public.show', $sheet) }}" class="button button--small">
            Ver sem acordes
          </a>
        </p>
      @else
        @canany(['gerir cânticos', 'ver acordes'])
          <p class="text-center">
            <a href="{{ route('songbook.public.show', $sheet) }}?acordes" class="button button--small">
              <span class="far fa-guitar mr-2"></span>
              Ver acordes
            </a>
          </p>
          @endcanany
          @endif
    @endcomponent

  </div>

@endsection
