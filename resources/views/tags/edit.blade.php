@extends('songbook::layouts.main')

@section('sb.content')
  <div class="sautor-addon__container">
    <header class="sautor-addon__header">
      <h1 class="sautor-addon__header__title">
        Editar etiqueta
      </h1>
    </header>

      @php($scope = 'edit-song')
      <form action="{{ route('songbook.etiquetas.update', $tag) }}" method="POST" data-vv-scope="{{ $scope }}">
        @method('PUT')
        @csrf

        @include('songbook::tags.form')

        <div class="sautor-addon__actions">
          <button type="submit" class="button button--primary">Editar</button>
          @can('delete', $tag)
            <button type="button" class="button button--danger-light" @click.prevent="openModal('deleteModal')">
              <span class="far fa-trash mr-2"></span>
              Eliminar
            </button>
          @endcan
        </div>

      </form>

  </div>

  @can('delete', $tag)
    <modal id="deleteModal">
      <div class="modal__body modal__confirmation">
        <div class="modal__confirmation__icon">
          <span class="fas fa-exclamation"></span>
        </div>
        <div class="modal__confirmation__content">
          <h3 class="modal__confirmation__title">Eliminar etiqueta</h3>
          <p class="modal__confirmation__text">
            Tem a certeza que quer eliminar a etiqueta <strong>{{ $tag->name }}</strong>?
          </p>
        </div>
      </div>
      <form class="modal__footer" method="POST" action="{{ route('songbook.etiquetas.destroy', $tag) }}">
        @csrf
        @method('DELETE')
        <button type="submit" class="button button--danger">Eliminar</button>
        <button type="button" class="button" @click.prevent="closeModal('deleteModal')">Cancelar</button>
      </form>
    </modal>
  @endcan
@endsection
