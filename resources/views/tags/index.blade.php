@extends('songbook::layouts.main')

@section('sb.content')
    <div class="sautor-addon__container">

        <header class="sautor-addon__header">
            <h1 class="sautor-addon__header__title">
                Etiquetas
            </h1>
            @can('create', \Sautor\Songbook\Models\Tag::class)
                <div class="sautor-addon__header__actions">
                    <a href="{{ route('songbook.etiquetas.create') }}" class="button button--responsive">
                        <span class="far fa-plus"></span>
                        Criar
                    </a>
                </div>
            @endcan
        </header>

        @include('songbook::tags.list-component')

    </div>
@endsection
