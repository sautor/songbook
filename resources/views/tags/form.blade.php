<x-forms.input name="name" label="Nome" :value="isset($tag) ? $tag->name : null" required :scope="$scope" />

<div class="field">
  <label class="label">Descrição</label>
  <div class="control flex-col">
    <small-content-editor name="description" value="{{ isset($tag) ? $tag->description : null }}" simple></small-content-editor>
  </div>
</div>

<div class="field">
  <label class="label">
    Grupo
  </label>
  <div class="control s-sb-flex-col">
    <grupo-picker name="grupo_id" with-input visiveis value="{{ old('grupo_id', (isset($tag) ? $tag->grupo_id : null)) }}"></grupo-picker>
  </div>
</div>