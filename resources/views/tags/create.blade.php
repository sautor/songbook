@extends('songbook::layouts.main')

@section('sb.content')
  <div class="sautor-addon__container">
    <header class="sautor-addon__header">
      <h1 class="sautor-addon__header__title">
        Criar etiqueta
      </h1>
    </header>

      @php($scope = 'new-tag')
      <form action="{{ route('songbook.etiquetas.store') }}" method="POST" data-vv-scope="{{ $scope }}">
        @csrf

        @include('songbook::tags.form')

        <div class="sautor-addon__actions">
          <button type="submit" class="button button--primary">Criar</button>
        </div>
      </form>
  </div>
@endsection
