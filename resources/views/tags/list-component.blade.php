@unless($tags->isEmpty())
  <section class="sautor-list tags-list">
    @foreach($tags as $tag)
      <div class="sautor-list__item">
        <div class="sautor-list__item__logo">
          @if($tag->grupo and $tag->grupo->logo)
            <img src="{{ $tag->grupo->logo }}" alt="{{ $tag->grupo->nome }}">
          @else
            <span class="sautor-list__item__logo__icon">
                <span class="fas fa-tag"></span>
            </span>
          @endif
        </div>
        <a href="{{ route('songbook.etiquetas.show', $tag) }}" class="sautor-list__item__data">
          <p class="sautor-list__item__name">{{ $tag->name }}</p>
          <p class="sautor-list__item__meta">
            {{ $tag->songs_count }} {{ $tag->songs_count === 1 ? 'cântico' : 'cânticos' }}
            @if($tag->grupo)
            &middot; {{ $tag->grupo->nome_curto }}
            @endif
          </p>
        </a>

        <div class="sautor-list__item__actions">
          @unless(empty($actions) or empty($a = $actions($pessoa)))
            @foreach($a as $action)
              <a title="{{ $action['label'] }}"
                 @if(!empty($action['link']))
                 href="{{ $action['link'] }}"
                 @elseif(!empty($action['modal']))
                 href="#" data-toggle="modal" data-target="#{{ $action['modal'] }}"
                      @endif
              >
                <span class="{{ $action['icon'] }}"></span>
              </a>
            @endforeach
          @endunless
          <a title="Ver" href="{{ route('songbook.etiquetas.show', $tag) }}">
            <span class="far fa-angle-right"></span>
          </a>
        </div>
      </div>
    @endforeach
  </section>
@else
  <section class="sautor-list--empty tags-list--empty">
    {{ empty($empty_message) ? 'Não existem etiquetas.' : $empty_message }}
  </section>
@endunless
