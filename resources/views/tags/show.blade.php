@extends('songbook::layouts.main')

@section('sb.content')
  <div class="sautor-addon__container">

    <header class="sautor-addon__header">
      <h1 class="sautor-addon__header__title">
        {{ $tag->name }}
        <small>Etiqueta</small>
      </h1>
      @can('update', $tag)
        <div class="sautor-addon__header__actions">
          <a href="{{ route('songbook.etiquetas.edit', $tag) }}" class="button button--responsive">
            <span class="far fa-pencil"></span>
            Editar
          </a>
        </div>
      @endcan
    </header>

      @unless(empty($tag->description))
      <div class="s-sb-text-lg s-sb-mb-4 -s-sb-mt-2 s-sb-font-accent">
        {!! $tag->description !!}
      </div>
      @endunless

      @if($tag->grupo)
        <p class="s-sb-mb-4"><strong>Grupo:</strong> {{ $tag->grupo->nome }}</p>
      @endif

      @include('songbook::songs.list-component')

    @if(method_exists($songs, 'links'))
      <div class="mt-4">
        {{ $songs->links() }}
      </div>
    @endif

  </div>
@endsection
