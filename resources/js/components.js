import vueCustomElement from 'vue-custom-element'
import SheetEditor from './components/SheetEditor.vue'
import DateCollectionInput from './components/DateCollectionInput.vue'
import Modal from '../../../core/resources/js/components/Modal.vue'
import modals from '../../../core/resources/js/mixins/modals'
import GrupoPicker from '../../../core/resources/js/components/GrupoPicker.vue'

import '../scss/styles.scss'

window.Vue.use(vueCustomElement);

window.Vue.component('Modal', Modal)
window.Vue.component('GrupoPicker', GrupoPicker)

window.Vue.customElement('s-sb-sheet-editor', SheetEditor, {
  beforeCreateVueInstance (rootElement) {
    rootElement.mixins = [modals]
    return rootElement
  }
})

window.Vue.customElement('s-sb-date-collection-input', DateCollectionInput, {
  beforeCreateVueInstance (rootElement) {
    rootElement.mixins = [modals]
    return rootElement
  }
})
