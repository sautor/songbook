<?php

Route::prefix('api/canticos')->middleware(['api', 'addon:songbook'])->name('api.songbook.')->namespace('Sautor\Songbook\Controllers')->group(function () {
    Route::get('canticos', 'SongsController@apiIndex');
    Route::get('canticos/{song}', 'SongsController@apiShow');
});
