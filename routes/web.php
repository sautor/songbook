<?php

Route::prefix('canticos')->middleware(['web', 'addon:songbook'])->name('songbook.')->namespace('Sautor\Songbook\Controllers')->group(function () {
    Route::middleware(['auth'])->group(function () {
        Route::resource('etiquetas', 'TagsController')->parameter('etiquetas', 'tag');
        Route::resource('canticos', 'SongsController')->parameter('canticos', 'song');
        Route::resource('folhas', 'SheetsController')->parameter('folhas', 'sheet');
    });

    Route::get('/', 'PublicController@index')->name('index');
    Route::get('{sheet}', 'PublicController@show')->name('public.show');
});
