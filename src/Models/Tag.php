<?php

namespace Sautor\Songbook\Models;

use Illuminate\Database\Eloquent\Model;
use Sautor\Core\Models\Grupo;
use Wildside\Userstamps\Userstamps;

class Tag extends Model
{
    use Userstamps;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sb_tags';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function grupo()
    {
        return $this->belongsTo(Grupo::class);
    }

    public function songs()
    {
        return $this->belongsToMany(Song::class, 'sb_song_tag');
    }
}
