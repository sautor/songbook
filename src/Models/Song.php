<?php

namespace Sautor\Songbook\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;
use Sautor\Core\Models\Grupo;
use Wildside\Userstamps\Userstamps;

class Song extends Model
{
    use Searchable;
    use SoftDeletes;
    use Userstamps;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sb_songs';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function grupo()
    {
        return $this->belongsTo(Grupo::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'sb_song_tag');
    }

    public function toSearchableArray()
    {
        return $this->only(['id', 'title', 'by', 'lyrics', 'tags']);
    }
}
