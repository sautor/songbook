<?php

namespace Sautor\Songbook\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Mtvs\EloquentHashids\HasHashid;
use Mtvs\EloquentHashids\HashidRouting;
use Sautor\Core\Models\Grupo;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Wildside\Userstamps\Userstamps;

class Sheet extends Model implements HasMedia
{
    use HasHashid, HashidRouting;
    use InteractsWithMedia;
    use Userstamps;

    const DATETIMES_TABLE = 'sb_sheet_times';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sb_sheets';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'content' => 'array',
        'is_public' => 'boolean',
    ];

    public function grupo()
    {
        return $this->belongsTo(Grupo::class);
    }

    public function getDatesAttribute()
    {
        return DB::table(self::DATETIMES_TABLE)
            ->where('sheet_id', $this->id)
            ->orderBy('datetime')
            ->pluck('datetime')
            ->map(function ($date) {
                return Carbon::parse($date);
            });
    }

    public function setDates($dates)
    {
        $dateCollection = collect($dates)->map(function ($date) {
            return $this->fromDateTime($date);
        });

        DB::transaction(function () use ($dateCollection) {
            // Delete existing wrong ones
            DB::table(self::DATETIMES_TABLE)
                ->where('sheet_id', $this->id)
                ->whereNotIn('datetime', $dateCollection)
                ->delete();

            // Get existing ones
            $existing = DB::table(self::DATETIMES_TABLE)
                ->where('sheet_id', $this->id)
                ->pluck('datetime');

            $newDates = $dateCollection->diff($existing);

            // Insert new ones
            DB::table(self::DATETIMES_TABLE)->insert(
                $newDates->map(function ($date) {
                    return [
                        'datetime' => $date,
                        'sheet_id' => $this->id,
                    ];
                })->all()
            );
        });
    }

    public function preparedContent()
    {
        $content = collect($this->content);
        $songIds = $content->where('type', 'song')->pluck('id');
        $songs = Song::whereIn('id', $songIds->all())->get();

        $content = $content->map(function ($item) use ($songs) {
            if ($item['type'] === 'song') {
                $item['song'] = $songs->firstWhere('id', $item['id']);
            } elseif ($item['type'] === 'groups') {
                $item['groups'] = Grupo::whereIn('id', $item['ids'])->get();
            }

            return $item;
        });

        $header = collect();

        while ($content->first()['type'] !== 'song') {
            $header->push($content->shift());
        }

        $footer = collect();

        while ($content->last()['type'] !== 'song') {
            $footer->prepend($content->pop());
        }

        $prepared = [
            'header' => $header,
            'main' => $content,
            'footer' => $footer,
        ];

        return $prepared;
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('pdf')
            ->acceptsMimeTypes(['application/pdf'])
            ->singleFile();

        $this->addMediaCollection('pdf-chords')
            ->acceptsMimeTypes(['application/pdf'])
            ->singleFile();
    }

    public function scopeNextWeek($query, $days = 7)
    {
        $minDate = Carbon::now()->sub(5, 'hours');
        $maxDate = Carbon::now()->endOfDay()->add($days, 'days');

        return $query->whereIn('id', function ($q) use ($minDate, $maxDate) {
            $q->select('sheet_id')
                ->from(self::DATETIMES_TABLE)
                ->where('datetime', '>=', $this->fromDateTime($minDate))
                ->where('datetime', '<', $this->fromDateTime($maxDate));
        })->orderBy(
            DB::table(self::DATETIMES_TABLE)
                ->selectRaw('min(datetime) as mindatetime')
                ->whereColumn('sheet_id', 'sb_sheets.id')
                ->where('datetime', '>=', $this->fromDateTime($minDate))
        );
    }

    public function scopePublic($query)
    {
        return $query->where('is_public', true);
    }
}
