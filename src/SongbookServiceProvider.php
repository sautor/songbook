<?php

namespace Sautor\Songbook;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Sautor\Core\Services\Addons\Addon;
use Sautor\Songbook\Models\Sheet;
use Sautor\Songbook\Models\Song;
use Sautor\Songbook\Models\Tag;
use Sautor\Songbook\Policies\SheetsPolicy;
use Sautor\Songbook\Policies\SongsPolicy;
use Sautor\Songbook\Policies\TagsPolicy;

class SongbookServiceProvider extends ServiceProvider
{
    protected $policies = [
        Sheet::class => SheetsPolicy::class,
        Song::class => SongsPolicy::class,
        Tag::class => TagsPolicy::class,
    ];

    private Addon $addon;

    /**
     * PaymentsServiceProvider constructor.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     */
    public function __construct($app)
    {
        parent::__construct($app);
        $this->addon = Addon::create('songbook', 'Cânticos', 'music', 'Extensão com livro de cânticos.')
            ->setGroupAddon(false)
            ->setManagersOnly(false)
            ->setEntryRoute('songbook.index')
            ->setRestrict(function (?Grupo $grupo) {
                return Sheet::nextWeek()->public()->doesntExist();
            })
            ->setPermissions(['gerir cânticos', 'ver acordes'])
            ->withAssets();
    }

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'songbook');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'songbook');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');

        $this->registerPolicies();

        $addonService = $this->app->make('AddonService');
        $addonService->register($this->addon);
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        //
    }

    public function registerPolicies()
    {
        foreach ($this->policies as $key => $value) {
            Gate::policy($key, $value);
        }
    }
}
