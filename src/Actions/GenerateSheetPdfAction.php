<?php

namespace Sautor\Songbook\Actions;

use Illuminate\Support\Facades\View;
use Sautor\Songbook\Models\Sheet;
use Spatie\Browsershot\Browsershot;

class GenerateSheetPdfAction
{
    protected Sheet $sheet;

    protected bool $chords;

    /**
     * GenerateSheetPdfAction constructor.
     */
    public function __construct(Sheet $sheet, bool $chords = false)
    {
        $this->sheet = $sheet;
        $this->chords = $chords;
    }

    public function execute()
    {
        $html = View::make('songbook::sheets.print', ['sheet' => $this->sheet, 'chords' => $this->chords, 'grupo' => $this->sheet->grupo])->render();

        $pdf = Browsershot::html($html)
            ->format('A4')
            ->waitUntilNetworkIdle()
            ->margins(10, 10, 10, 10)
            ->newHeadless()
            ->noSandbox()
            ->showBackground();
        $this->sheet->addMediaFromBase64($pdf->base64pdf())
            ->usingName($this->sheet->title.($this->chords ? ' - Acordes' : ''))
            ->usingFileName('canticos-'.$this->sheet->hashid().($this->chords ? '-acordes' : '').'.pdf')
            ->toMediaCollection($this->chords ? 'pdf-chords' : 'pdf');
    }
}
