<?php

namespace Sautor\Songbook\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Sautor\Core\Models\Pessoa;
use Sautor\Songbook\Models\Song;

class SongsPolicy
{
    use HandlesAuthorization;

    public function before(?Pessoa $user, $ability)
    {
        if ($user and $user->can('gerir cânticos')) {
            return true;
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @return mixed
     */
    public function viewAny(Pessoa $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @return mixed
     */
    public function view(Pessoa $user, Song $song)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @return mixed
     */
    public function create(Pessoa $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @return mixed
     */
    public function update(Pessoa $user, Song $song)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @return mixed
     */
    public function delete(Pessoa $user, Song $song)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @return mixed
     */
    public function restore(Pessoa $user, Song $song)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @return mixed
     */
    public function forceDelete(Pessoa $user, Song $song)
    {
        //
    }
}
