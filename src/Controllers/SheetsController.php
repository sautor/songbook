<?php

namespace Sautor\Songbook\Controllers;

use Filament\Notifications\Notification;
use Illuminate\Http\Request;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Songbook\Actions\GenerateSheetPdfAction;
use Sautor\Songbook\Models\Sheet;

use function Sentry\captureException;

class SheetsController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Sheet::class, 'sheet');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sheets = Sheet::latest()->paginate();

        return view('songbook::sheets.index', compact('sheets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('songbook::sheets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'grupo_id' => 'exists:grupos,id|nullable',
            'content' => 'required|json',
            'datetimes.*' => 'date|nullable',
        ]);

        $data = $request->only('title', 'grupo_id', 'is_public');
        $data['content'] = json_decode($request->input('content'));

        $sheet = Sheet::create($data);

        $sheet->setDates(json_decode($request->input('datetimes')));

        try {
            (new GenerateSheetPdfAction($sheet))->execute();
            (new GenerateSheetPdfAction($sheet, true))->execute();
            Notification::make()
                ->title('Folha de cânticos criada com sucesso.')
                ->success()
                ->send();
        } catch (\Exception $e) {
            captureException($e);
            Notification::make()
                ->title('A folha de cânticos foi criada com sucesso mas ocorreu um erro ao gerar o PDF.')
                ->warning()
                ->send();
        }

        return redirect(route('songbook.folhas.show', $sheet));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Sheet $sheet)
    {
        $content = $sheet->preparedContent();

        return view('songbook::sheets.show', compact('sheet', 'content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Sheet $sheet)
    {
        return view('songbook::sheets.edit', compact('sheet'))->with('formModel', $sheet);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sheet $sheet)
    {
        $this->validate($request, [
            'title' => 'required',
            'grupo_id' => 'exists:grupos,id|nullable',
            'content' => 'required|json',
            'datetimes.*' => 'date|nullable',
        ]);

        $data = $request->only('title', 'grupo_id', 'is_public');
        $data['content'] = json_decode($request->input('content'));

        $sheet->update($data);

        $sheet->setDates(json_decode($request->input('datetimes')));

        try {
            (new GenerateSheetPdfAction($sheet))->execute();
            (new GenerateSheetPdfAction($sheet, true))->execute();
            Notification::make()
                ->title('Folha de cânticos editada com sucesso.')
                ->success()
                ->send();
        } catch (\Exception $e) {
            captureException($e);
            Notification::make()
                ->title('A folha de cânticos foi editada com sucesso mas ocorreu um erro ao gerar o PDF.')
                ->warning()
                ->send();
        }

        return redirect(route('songbook.folhas.show', $sheet));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sheet $sheet)
    {
        $sheet->delete();

        Notification::make()
            ->title('Folha de cânticos eliminada com sucesso.')
            ->success()
            ->send();

        return redirect(route('songbook.folhas.index'));
    }
}
