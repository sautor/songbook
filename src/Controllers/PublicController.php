<?php

namespace Sautor\Songbook\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Songbook\Models\Sheet;

class PublicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sheets = Sheet::nextWeek()->public()->get();

        return view('songbook::public.index', compact('sheets'));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Sheet $sheet)
    {
        $this->authorize('view', $sheet);
        $chords = $request->has('acordes');
        if ($chords && (Auth::guest() || ! Auth::user()->hasAnyPermission(['gerir cânticos', 'ver acordes']))) {
            abort(403);

            return;
        }

        return view('songbook::public.show', ['sheet' => $sheet, 'chords' => $chords]);
    }
}
