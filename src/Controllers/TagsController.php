<?php

namespace Sautor\Songbook\Controllers;

use Filament\Notifications\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Songbook\Models\Tag;

class TagsController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Tag::class, 'tag');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::withCount('songs')->orderBy('name')->get();

        return view('songbook::tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('songbook::tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'grupo_id' => 'exists:grupos,id|nullable',
        ]);

        Tag::create($request->all());

        Notification::make()
            ->title('Etiqueta criada com sucesso.')
            ->success()
            ->send();

        return redirect(route('songbook.etiquetas.index'));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        $songs = $tag->songs()->orderBy('title')->paginate();

        return view('songbook::tags.show', compact('tag', 'songs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        return view('songbook::tags.edit', compact('tag'))->with('formModel', $tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        $this->validate($request, [
            'name' => 'required',
            'grupo_id' => 'exists:grupos,id|nullable',
        ]);

        $tag->update($request->all());

        Notification::make()
            ->title('Etiqueta editada com sucesso.')
            ->success()
            ->send();

        return redirect(route('songbook.etiquetas.show', $tag));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();

        Notification::make()
            ->title('Etiqueta eliminada com sucesso.')
            ->success()
            ->send();

        return redirect(route('songbook.etiquetas.index'));
    }
}
